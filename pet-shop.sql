-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2023 at 10:32 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pet-shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `name_customer` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`customer_id`, `name_customer`, `customer_address`, `phone_number`, `email`) VALUES
(1, 'Sakti Permadi', 'Ds. Abdullah No. 955, Mataram 44507, Kalbar', '(+62) 385 1858 2363', 'kamila72@yahoo.co.id'),
(2, 'Tami Titi Rahimah M.Kom.', 'Jr. Banceng Pondok No. 477, Solok 23727, Sumbar', '(+62) 936 2909 0546', 'hsihotang@gmail.co.id'),
(3, 'Cakrawala Gamani Haryanto M.Kom.', 'Gg. Bhayangkara No. 539, Sibolga 25870, Sultra', '(+62) 24 4410 579', 'lestari.okta@najmudin.my.id'),
(4, 'Hana Cindy Uyainah S.T.', 'Gg. Pattimura No. 45, Pariaman 28554, Sumbar', '(+62) 560 9058 9410', 'mardhiyah.ella@yahoo.com'),
(5, 'Maida Riyanti', 'Jln. Panjaitan No. 290, Bandar Lampung 41787, DKI', '0243 4950 7840', 'prastuti.limar@sitorus.web.id'),
(6, 'Hamzah Radika Budiyanto', 'Psr. Salak No. 407, Kendari 18535, Sultra', '0345 4062 2412', 'maimunah82@puspita.my.id'),
(7, 'Gaiman Cengkal Prabowo S.Psi', 'Ki. Kalimantan No. 189, Tasikmalaya 12330, Malut', '0715 2890 062', 'dirja.haryanti@latupono.ac.id'),
(8, 'Tira Farida', 'Psr. Bakit  No. 893, Kendari 95987, DIY', '0217 1792 288', 'uyainah.luthfi@yahoo.com'),
(9, 'Hilda Ina Anggraini S.I.Kom', 'Jr. Agus Salim No. 782, Ambon 97710, Sulteng', '(+62) 834 362 592', 'rriyanti@samosir.asia'),
(10, 'Endra Asmadi Simbolon', 'Ki. Bambu No. 668, Tual 58581, DIY', '(+62) 843 813 497', 'danuja24@yahoo.com'),
(11, 'Yessi Kamila Nurdiyanti M.M.', 'Gg. Basuki Rahmat  No. 861, Bau-Bau 80319, Babel', '(+62) 665 3740 070', 'xbudiman@nababan.web.id'),
(12, 'Gilda Clara Utami S.Pd', 'Ds. S. Parman No. 814, Administrasi Jakarta Selatan 35666, Banten', '(+62) 340 2275 3464', 'belinda33@irawan.info'),
(13, 'Tina Sadina Nuraini S.Ked', 'Kpg. Suniaraja No. 190, Pematangsiantar 51924, Kaltim', '(+62) 357 7517 9036', 'utami.lutfan@yahoo.co.id'),
(14, 'Marsito Situmorang S.Ked', 'Psr. Wahid Hasyim No. 607, Dumai 69763, Kalbar', '0834 717 539', 'simbolon.marwata@gmail.com'),
(15, 'Heru Sirait', 'Ds. Abdullah No. 929, Sibolga 86636, Kalbar', '(+62) 830 5626 4129', 'nainggolan.vinsen@yahoo.co.id'),
(16, 'Padma Ifa Pratiwi', 'Psr. Bakin No. 352, Medan 41505, Sultra', '0978 3344 2916', 'sinaga.cawisono@mangunsong.ac.id'),
(17, 'Hafshah Winarsih S.Pt', 'Ds. Pelajar Pejuang 45 No. 230, Palembang 12170, Jateng', '(+62) 27 2052 221', 'febi.firmansyah@gmail.com'),
(18, 'Janet Wijayanti M.M.', 'Psr. Suryo No. 295, Pangkal Pinang 65855, Kaltara', '020 7600 0507', 'aaryani@puspasari.com'),
(19, 'Mulya Tomi Putra M.Ak', 'Ds. Peta No. 654, Bekasi 41992, DIY', '0451 0692 3547', 'samsul52@yahoo.co.id'),
(20, 'Shakila Hassanah S.IP', 'Dk. Kyai Gede No. 191, Jayapura 92322, Jabar', '(+62) 530 3451 331', 'vriyanti@prasetyo.co'),
(21, 'Rendy Jaiman Damanik S.Sos', 'Kpg. Barasak No. 41, Balikpapan 70588, Gorontalo', '0492 6533 0696', 'hastuti.himawan@gmail.co.id'),
(22, 'Nadia Natalia Nurdiyanti S.Psi', 'Gg. Abdul Rahmat No. 6, Lhokseumawe 67145, Jatim', '0661 1341 0470', 'xsetiawan@sudiati.go.id'),
(23, 'Emong Hartaka Firgantoro S.Ked', 'Ki. Bata Putih No. 85, Parepare 75482, Aceh', '0267 5042 2613', 'mahendra.rahman@gmail.com'),
(24, 'Halima Pertiwi', 'Jr. Asia Afrika No. 177, Samarinda 52960, Sulut', '(+62) 23 0789 283', 'tantri33@gmail.com'),
(25, 'Artawan Soleh Nashiruddin M.M.', 'Ds. Pelajar Pejuang 45 No. 876, Banjarmasin 25092, Riau', '(+62) 596 4546 336', 'ophelia.hutagalung@winarno.net'),
(26, 'Nyoman Samosir', 'Jr. Otista No. 516, Banjar 35579, Sulteng', '0620 8577 6499', 'vusamah@gmail.co.id'),
(27, 'Cinta Jasmin Susanti', 'Psr. Jakarta No. 110, Depok 66590, Jatim', '(+62) 727 2267 726', 'hamzah.purnawati@kusumo.web.id'),
(28, 'Fathonah Kayla Usamah', 'Ds. Taman No. 137, Parepare 78127, Kalsel', '0341 9888 3375', 'kasim.hutapea@mardhiyah.net'),
(29, 'Umi Tina Anggraini', 'Ds. Muwardi No. 922, Pematangsiantar 46157, Sulbar', '0723 5324 5942', 'padma60@gmail.com'),
(30, 'Najwa Talia Purnawati S.IP', 'Ki. Yap Tjwan Bing No. 738, Mataram 44634, Pabar', '(+62) 381 4284 4926', 'eva01@wulandari.or.id'),
(31, 'Hendri Kurniawan S.IP', 'Psr. Salak No. 332, Batu 70986, Sumsel', '(+62) 223 1349 012', 'marsito41@nurdiyanti.my.id'),
(32, 'Atmaja Maulana S.Farm', 'Gg. Banda No. 363, Tanjung Pinang 65255, Sulsel', '(+62) 386 8570 531', 'vkuswandari@yahoo.com'),
(33, 'Nurul Lestari M.Farm', 'Gg. Bakau Griya Utama No. 623, Payakumbuh 27668, Kaltara', '(+62) 713 0348 1260', 'elvina72@gmail.co.id'),
(34, 'Janet Nilam Pertiwi', 'Jr. Kyai Gede No. 661, Sawahlunto 74562, Kepri', '0412 0055 232', 'natalia.napitupulu@yahoo.com'),
(35, 'Zulaikha Wahyuni M.Ak', 'Kpg. Moch. Ramdan No. 287, Tarakan 53087, DKI', '(+62) 955 9361 155', 'balamantri.habibi@astuti.ac.id'),
(36, 'Artanto Adika Tarihoran', 'Dk. Achmad No. 172, Ternate 40413, Sulut', '0911 6631 668', 'maryanto91@gmail.co.id'),
(37, 'Lukita Jailani', 'Gg. Baranang Siang No. 430, Banjarmasin 42662, Bengkulu', '0208 4345 667', 'ysusanti@gmail.co.id'),
(38, 'Lidya Laksmiwati', 'Jln. Warga No. 95, Lhokseumawe 75519, Sulteng', '0679 1878 0150', 'harimurti.usada@yahoo.com'),
(39, 'Hadi Umaya Salahudin M.Farm', 'Jln. Salatiga No. 959, Sorong 44114, Riau', '0214 5790 339', 'pratama.michelle@gmail.com'),
(40, 'Legawa Hardana Irawan S.H.', 'Gg. Fajar No. 332, Surakarta 98751, DKI', '0637 3107 516', 'gandi27@sihotang.go.id'),
(41, 'Yani Hastuti', 'Ds. Ekonomi No. 177, Denpasar 13659, Kepri', '0788 2375 574', 'yyuniar@nasyiah.biz'),
(42, 'Lalita Hasanah', 'Jln. HOS. Cjokroaminoto (Pasirkaliki) No. 621, Tegal 96817, Bali', '(+62) 227 9912 680', 'dartono.kusmawati@lestari.id'),
(43, 'Yani Agustina', 'Psr. Bara Tambar No. 652, Palembang 12541, Kepri', '029 3206 4417', 'usada.elvina@yahoo.co.id'),
(44, 'Talia Haryanti', 'Gg. Siliwangi No. 480, Tarakan 88851, NTB', '(+62) 839 0392 2262', 'umi.suartini@yahoo.co.id'),
(45, 'Rizki Saragih', 'Ds. Bacang No. 481, Dumai 48233, Kalteng', '028 2401 630', 'nababan.rahayu@sihotang.asia'),
(46, 'Sari Usamah S.Ked', 'Jr. Baung No. 982, Madiun 74704, Sulteng', '(+62) 529 5585 917', 'zprakasa@yahoo.com'),
(47, 'Ganda Adriansyah', 'Dk. Camar No. 613, Bandung 50873, Sulbar', '(+62) 574 6337 401', 'vanesa83@laksita.sch.id'),
(48, 'Kamila Nurul Palastri S.Pt', 'Jr. Bambu No. 873, Bitung 45009, Sumut', '0833 648 861', 'umi29@usamah.or.id'),
(49, 'Yani Agustina S.Psi', 'Psr. Warga No. 56, Banda Aceh 35478, NTT', '0612 4463 0425', 'salimah22@gmail.com'),
(50, 'Ian Emas Irawan', 'Ds. Reksoninten No. 828, Denpasar 44689, Bengkulu', '0700 2384 3044', 'tusada@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaction`
--

CREATE TABLE `detail_transaction` (
  `detail_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `total_product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_transaction`
--

INSERT INTO `detail_transaction` (`detail_transaction_id`, `customer_id`, `product_id`, `total_product`) VALUES
(1, 43, 31, 170),
(2, 19, 18, 169),
(3, 28, 15, 801),
(4, 6, 27, 140),
(5, 42, 42, 589),
(6, 6, 21, 380),
(7, 21, 2, 103),
(8, 16, 37, 612),
(9, 31, 24, 138),
(10, 36, 41, 779),
(11, 49, 2, 345),
(12, 20, 49, 854),
(13, 24, 3, 369),
(14, 8, 1, 960),
(15, 15, 26, 766),
(16, 13, 1, 281),
(17, 45, 4, 444),
(18, 48, 4, 716),
(19, 50, 5, 381),
(20, 25, 17, 305),
(21, 40, 24, 195),
(22, 23, 13, 788),
(23, 47, 9, 904),
(24, 28, 19, 960),
(25, 20, 43, 494),
(26, 38, 44, 11),
(27, 7, 26, 539),
(28, 32, 8, 637),
(29, 49, 31, 767),
(30, 17, 5, 188),
(31, 38, 39, 825),
(32, 46, 43, 65),
(33, 46, 30, 324),
(34, 13, 9, 602),
(35, 28, 12, 123),
(36, 40, 33, 96),
(37, 24, 36, 39),
(38, 39, 45, 284),
(39, 6, 6, 88),
(40, 12, 23, 298),
(41, 30, 5, 178),
(42, 17, 22, 717),
(43, 19, 30, 29),
(44, 24, 36, 795),
(45, 16, 16, 725),
(46, 9, 23, 270),
(47, 46, 17, 532),
(48, 9, 14, 881),
(49, 6, 29, 737),
(50, 46, 31, 224);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(2, '2023_04_02_043942_create_customers_table', 1),
(3, '2023_04_02_045523_create_table_pets', 1),
(4, '2023_04_02_052927_create_table_products', 1),
(5, '2023_04_02_053342_create_table_transactions', 1),
(6, '2023_04_02_055108_create_table_detail_transaction', 1);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pets`
--

CREATE TABLE `pets` (
  `pet_id` int(11) NOT NULL,
  `pet_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `weight` double(8,2) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pets`
--

INSERT INTO `pets` (`pet_id`, `pet_type`, `age`, `weight`, `customer_id`) VALUES
(1, 'Bird', 4, 4.00, 10),
(2, 'Dog', 3, 6.00, 21),
(3, 'Bird', 2, 7.00, 29),
(4, 'Hamster', 3, 5.00, 40),
(5, 'Sugar Glider', 5, 3.00, 48),
(6, 'Cat', 1, 4.00, 49),
(7, 'Sugar Glider', 8, 5.00, 4),
(8, 'Cat', 3, 6.00, 38),
(9, 'Hamster', 2, 7.00, 2),
(10, 'Dog', 10, 3.00, 28),
(11, 'Dog', 4, 1.00, 48),
(12, 'Bird', 3, 6.00, 22),
(13, 'Cat', 5, 9.00, 11),
(14, 'Hamster', 3, 5.00, 20),
(15, 'Sugar Glider', 9, 8.00, 46),
(16, 'Cat', 4, 8.00, 2),
(17, 'Cat', 4, 5.00, 32),
(18, 'Dog', 10, 7.00, 33),
(19, 'Dog', 6, 3.00, 10),
(20, 'Dog', 7, 8.00, 15),
(21, 'Hamster', 7, 4.00, 4),
(22, 'Fish', 9, 2.00, 22),
(23, 'Dog', 2, 3.00, 42),
(24, 'Fish', 7, 9.00, 19),
(25, 'Dog', 2, 4.00, 46),
(26, 'Fish', 9, 4.00, 42),
(27, 'Bird', 4, 2.00, 27),
(28, 'Sugar Glider', 8, 5.00, 30),
(29, 'Hamster', 4, 2.00, 1),
(30, 'Sugar Glider', 8, 5.00, 9),
(31, 'Hamster', 9, 7.00, 29),
(32, 'Hamster', 3, 5.00, 2),
(33, 'Bird', 2, 1.00, 42),
(34, 'Fish', 6, 1.00, 5),
(35, 'Bird', 9, 8.00, 41),
(36, 'Dog', 3, 4.00, 38),
(37, 'Bird', 7, 1.00, 34),
(38, 'Dog', 6, 6.00, 32),
(39, 'Sugar Glider', 8, 4.00, 7),
(40, 'Sugar Glider', 1, 9.00, 45),
(41, 'Sugar Glider', 8, 4.00, 8),
(42, 'Cat', 6, 8.00, 23),
(43, 'Bird', 5, 2.00, 13),
(44, 'Bird', 5, 2.00, 1),
(45, 'Dog', 2, 8.00, 24),
(46, 'Cat', 10, 1.00, 42),
(47, 'Fish', 10, 4.00, 26),
(48, 'Dog', 7, 6.00, 2),
(49, 'Fish', 2, 6.00, 28),
(50, 'Fish', 3, 1.00, 8);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `product_type`, `price`) VALUES
(1, 'Royal Canin', 'vitamin', 37019),
(2, 'Jolly', 'accessories', 37601),
(3, 'SUper Cat', 'Pet Toys', 49897),
(4, 'Jolly', 'accessories', 21712),
(5, 'HamsFood', 'Pet Meal', 53333),
(6, 'SUper Cat', 'Pet Toys', 22721),
(7, 'SUper Cat', 'Pet Meal', 29929),
(8, 'HamsFood', 'accessories', 50050),
(9, 'SUper Cat', 'Pet Meal', 81730),
(10, 'Royal Canin', 'Pet Meal', 71142),
(11, 'Royal Canin', 'Pet Toys', 24199),
(12, 'HamsFood', 'Pet Toys', 60051),
(13, 'HamsFood', 'Pet Meal', 61132),
(14, 'HamsFood', 'Pet Toys', 16536),
(15, 'SUper Cat', 'Pet Toys', 82448),
(16, 'Royal Canin', 'vitamin', 67542),
(17, 'SUper Cat', 'vitamin', 18420),
(18, 'SUper Cat', 'Pet Meal', 38292),
(19, 'HamsFood', 'accessories', 50833),
(20, 'SUper Cat', 'Pet Meal', 86151),
(21, 'Jolly', 'Pet Toys', 74280),
(22, 'Jolly', 'vitamin', 30274),
(23, 'HamsFood', 'Pet Meal', 70683),
(24, 'Jolly', 'vitamin', 32794),
(25, 'HamsFood', 'Pet Meal', 67457),
(26, 'Royal Canin', 'accessories', 31454),
(27, 'HamsFood', 'vitamin', 32504),
(28, 'Royal Canin', 'vitamin', 59886),
(29, 'Jolly', 'accessories', 53625),
(30, 'Jolly', 'vitamin', 57106),
(31, 'HamsFood', 'accessories', 22340),
(32, 'SUper Cat', 'Pet Meal', 26461),
(33, 'Royal Canin', 'vitamin', 54865),
(34, 'Royal Canin', 'accessories', 14663),
(35, 'Jolly', 'Pet Meal', 19438),
(36, 'SUper Cat', 'Pet Toys', 39964),
(37, 'HamsFood', 'Pet Meal', 69759),
(38, 'HamsFood', 'Pet Meal', 34257),
(39, 'Jolly', 'vitamin', 12532),
(40, 'Jolly', 'accessories', 90758),
(41, 'HamsFood', 'Pet Toys', 61909),
(42, 'Jolly', 'Pet Meal', 24466),
(43, 'Jolly', 'accessories', 51181),
(44, 'HamsFood', 'vitamin', 85498),
(45, 'SUper Cat', 'accessories', 68222),
(46, 'SUper Cat', 'vitamin', 33669),
(47, 'SUper Cat', 'Pet Meal', 84203),
(48, 'SUper Cat', 'Pet Meal', 29929),
(49, 'SUper Cat', 'accessories', 53178),
(50, 'Royal Canin', 'Pet Meal', 65138);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `transaction_id` int(11) NOT NULL,
  `transaction_date` date NOT NULL,
  `total_price` bigint(20) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`transaction_id`, `transaction_date`, `total_price`, `customer_id`) VALUES
(1, '2000-02-21', 428126, 12),
(2, '1976-10-03', 293608, 44),
(3, '1994-12-22', 542751, 48),
(4, '2019-02-08', 825723, 45),
(5, '2020-10-11', 427855, 43),
(6, '1974-03-27', 956040, 48),
(7, '2001-02-26', 413271, 14),
(8, '1994-11-01', 307342, 3),
(9, '1979-10-05', 493099, 38),
(10, '2014-03-16', 95209, 15),
(11, '1974-04-03', 465206, 28),
(12, '2008-05-14', 759184, 26),
(13, '1973-11-14', 780972, 22),
(14, '1990-03-05', 544626, 32),
(15, '2002-09-25', 151626, 1),
(16, '1979-05-26', 309776, 44),
(17, '1998-03-05', 271929, 3),
(18, '1991-09-26', 507216, 19),
(19, '1999-03-04', 307769, 23),
(20, '1979-12-30', 145461, 22),
(21, '1972-06-04', 642163, 8),
(22, '1985-10-19', 595828, 48),
(23, '1971-03-05', 388311, 18),
(24, '1990-09-07', 526562, 36),
(25, '2004-05-11', 609018, 2),
(26, '2022-02-23', 961239, 50),
(27, '1999-09-22', 507739, 42),
(28, '2013-07-10', 950513, 16),
(29, '1970-07-11', 739177, 16),
(30, '2007-04-15', 834588, 20),
(31, '1976-01-25', 828096, 18),
(32, '2018-02-08', 881817, 18),
(33, '2004-07-28', 764795, 45),
(34, '1992-08-18', 423542, 41),
(35, '1979-05-25', 620639, 9),
(36, '2014-07-03', 90092, 6),
(37, '1971-07-06', 43496, 19),
(38, '1972-07-21', 374931, 9),
(39, '2005-07-10', 133027, 11),
(40, '2017-09-09', 347416, 6),
(41, '2000-11-12', 81160, 23),
(42, '1988-11-13', 857120, 42),
(43, '1982-05-23', 27335, 3),
(44, '1977-01-02', 74547, 32),
(45, '2004-09-29', 980441, 10),
(46, '1991-12-02', 791200, 21),
(47, '1991-06-17', 682345, 15),
(48, '1973-03-14', 560273, 4),
(49, '2016-03-07', 389448, 30),
(50, '1991-07-18', 259652, 38);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `detail_transaction`
--
ALTER TABLE `detail_transaction`
  ADD PRIMARY KEY (`detail_transaction_id`),
  ADD KEY `detail_transaction_customer_id_foreign` (`customer_id`),
  ADD KEY `detail_transaction_product_id_foreign` (`product_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `pets`
--
ALTER TABLE `pets`
  ADD PRIMARY KEY (`pet_id`),
  ADD KEY `pets_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`transaction_id`),
  ADD KEY `transactions_customer_id_foreign` (`customer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `detail_transaction`
--
ALTER TABLE `detail_transaction`
  MODIFY `detail_transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pets`
--
ALTER TABLE `pets`
  MODIFY `pet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_transaction`
--
ALTER TABLE `detail_transaction`
  ADD CONSTRAINT `detail_transaction_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`),
  ADD CONSTRAINT `detail_transaction_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);

--
-- Constraints for table `pets`
--
ALTER TABLE `pets`
  ADD CONSTRAINT `pets_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
